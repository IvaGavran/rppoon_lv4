﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks_3_4_and_5
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();
    }
}