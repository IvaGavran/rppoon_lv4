﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks_3_4_and_5
{
    //task no. 4:
    //Pitanje: Uočavate li ikakve razlike? 
    //Generalna razlika je u tome što su ovoga puta naši proizvodi omotani u dekorater pa im se ponašanje razlikuje;
    //proizvodi su označeni kao trending te im je povećana cijena, no sam ispis se ništa posebno ne razlikuje, 
    //trending proizvodi normalno su nadodani u našu listu, klijent ne uočava posebnu razliku.
    class Program
    {
        static void Main(string[] args)
        {
            //task no. 3:
            var rentableItems = new List<IRentable>();
            IRentable firstBook = new Book("First book");
            IRentable firstVideo = new Video("First video");
            rentableItems.Add(firstBook);
            rentableItems.Add(firstVideo);
            RentingConsolePrinter consolePrinter = new RentingConsolePrinter();
            consolePrinter.PrintTotalPrice(rentableItems);
            consolePrinter.DisplayItems(rentableItems);

            //task no. 4:
            IRentable hotBook = new HotItem(new Book("Second book"));
            IRentable hotVideo = new HotItem(new Video("Second video"));
            rentableItems.Add(hotBook);
            rentableItems.Add(hotVideo);
            Console.WriteLine();
            consolePrinter.PrintTotalPrice(rentableItems);
            consolePrinter.DisplayItems(rentableItems);

            //task no. 5:
            var flashSale = new List<IRentable>();
            foreach (IRentable item in rentableItems)
            {
                flashSale.Add(new DiscountedItem(item, 20));
            }
            Console.WriteLine();
            consolePrinter.PrintTotalPrice(flashSale);
            consolePrinter.DisplayItems(flashSale);
        }
    }
}
