﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks_3_4_and_5
{
    //task no. 5:
    class DiscountedItem : RentableDecorator
    {
        private double disconuntPercentage = 0.0;
        public DiscountedItem(IRentable rentable, double discountPercentage) : base(rentable)
        {
            this.disconuntPercentage = discountPercentage;
        }
        public override double CalculatePrice()
        {
            return base.CalculatePrice() - base.CalculatePrice() * (this.disconuntPercentage/100);
        }
        public override String Description
        {
            get
            {
                return base.Description + " now at " + this.disconuntPercentage + " % off!";
            }
        }
    }
}