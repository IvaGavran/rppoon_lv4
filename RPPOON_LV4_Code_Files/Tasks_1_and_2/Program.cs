﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks_1_and_2
{
    class Program
    {
        static void Main(string[] args)
        {
            //task no. 2:
            Dataset dataset = new Dataset("C:\\Users\\Kivi\\source\\repos\\RPPOON_LV4\\File.csv");
            Analyzer3rdParty analyzer = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer);
            double[] averagePerRow = adapter.CalculateAveragePerRow(dataset);
            double[] averagePerColumn = adapter.CalculateAveragePerColumn(dataset);

            Console.WriteLine("Average per row:");
            for (int i = 0; i < averagePerRow.Length; i++)
            {
                Console.WriteLine(averagePerRow[i]);
            }
            Console.WriteLine("Average per column:");
            for (int i = 0; i < averagePerColumn.Length; i++)
            {
                Console.WriteLine(averagePerColumn[i]);
            }
        }
    }
}
