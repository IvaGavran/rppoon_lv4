﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks_6_and_7
{
    //task no. 7:
    class RegistrationValidator : IRegistrationValidator
    {
        public bool IsUserEntryValid(UserEntry entry)
        {
            int minPasswordLength = 5;
            EmailValidator emailValidator = new EmailValidator();
            PasswordValidator passwordValidator = new PasswordValidator(minPasswordLength);
            return emailValidator.IsValidAddress(entry.Email) && passwordValidator.IsValidPassword(entry.Password);     
        }
    }
}
