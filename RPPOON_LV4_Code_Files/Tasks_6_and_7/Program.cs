﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks_6_and_7
{
    class Program
    {
        static void Main(string[] args)
        {
            //task no. 6:
            int minPasswordLength = 5;

            EmailValidator emailValidator = new EmailValidator();
            Console.WriteLine(emailValidator.IsValidAddress("ivagavran@domain.hr"));
            Console.WriteLine(emailValidator.IsValidAddress("ivagavran@domain.com"));
            Console.WriteLine(emailValidator.IsValidAddress("ivagavrandomain.hr"));
            Console.WriteLine(emailValidator.IsValidAddress("ivagavran@domaincom"));

            PasswordValidator passwordValidator = new PasswordValidator(minPasswordLength);
            Console.WriteLine(passwordValidator.IsValidPassword("ValidPassword1"));
            Console.WriteLine(passwordValidator.IsValidPassword("123"));
            Console.WriteLine(passwordValidator.IsValidPassword("onlyLetters"));
            Console.WriteLine(passwordValidator.IsValidPassword(" "));

            //task no. 7:
            RegistrationValidator registrationValidator = new RegistrationValidator();
            UserEntry userEntry;
            bool isEntryValid = false;
            while (isEntryValid == false)
            { 
                userEntry= UserEntry.ReadUserFromConsole();
                isEntryValid = registrationValidator.IsUserEntryValid(userEntry);
            }
        }
    }
}
