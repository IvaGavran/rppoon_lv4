﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tasks_6_and_7
{
    //task no. 6:
    class EmailValidator : IEmailValidatorService
    {
        public bool IsValidAddress(String candidate)
        {
            if (String.IsNullOrEmpty(candidate))
            {
                return false;
            }
            return ContainsHrOrComEnding(candidate) && ContainsAtSign(candidate);
        }
        private bool ContainsHrOrComEnding(String candidate)
        {
            bool dotHr = false, dotCom = false;
            if (candidate.Contains(".hr") && candidate.EndsWith(".hr")) dotHr = true;
            if (candidate.Contains(".com") && candidate.EndsWith(".com")) dotCom = true;
            return dotHr || dotCom;
        }
        private bool ContainsAtSign(string candidate)
        {
            bool hasAtSign = false;
            foreach (char c in candidate)
            {
                if (c == '@') hasAtSign = true;
            }
            return hasAtSign;
        }
    }
}